const express = require("express");
const router = express.Router();
const auth = require("../auth")
const courseController = require("../controllers/courseController");



// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(userData.isAdmin == true){

	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
} else {
	return res.send("NOT ADMIN")
}
});


// Route for retrieving all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

// Retrieve all the active courses
router.get("/active", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Retrieve specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});


// Update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})


// Activity S40 Archiving
router.patch("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})



module.exports = router;