const Course = require("../models/Course");

module.exports.addCourse = (reqBody) => {

	
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
};

// Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
};

// Retrieve all the active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
};

// Retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

// Update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	/*
	SYNTAX:
	findByIDandUpdate(document ID, updatesToBeApplied)
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {
		if (error){
			return false;
		} else {
			return true;
		};
	});
};

// Activity S40 Archiving
module.exports.archiveCourse = (reqParams, reqBody) => {
	let archiveCourse = {
		isActive: reqBody.isActive
	};
	return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((course, error) => {
		if (error){
			return false;
		} else {
			return true;
		};
	});
};


